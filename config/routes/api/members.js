const express = require('express');
const router = express.Router();

const MembersController = require('../../../app/controllers/api/members_controller');

router
    .route('/')
        .get(MembersController.get_members);

router
    .route('/:id')
        .get(MembersController.get_member);

module.exports = router;