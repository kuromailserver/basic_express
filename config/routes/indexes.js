const path = require('path');

const express = require('express');
const router = express.Router();

const IndexesController = require('../../app/controllers/indexes_controller');

router
    .route('/')
        .get(IndexesController.get_index)
        .post(IndexesController.post_data);

router
    .route('/get_index_from_db')
        .get(IndexesController.get_index_from_db)
        
// file upload

module.exports = router;