const urls = [
    {
      id: 1,
      type:'Mini',
      name: 'index',
      url: 'http://192.168.33.10:3000'
    },
    {
      id: 2,
      type:'Mini',
      name: 'get_index_from_db',
      url: 'http://192.168.33.10:3000/get_index_from_db'
    },
    {
      id: 3,
      type:'Mini',
      name: 'api members',
      url: 'http://192.168.33.10:3000/api/members'
    },
    {
      id: 4,
      type:'Mini',
      name: 'api member 1',
      url: 'http://192.168.33.10:3000/api/members/1'
    }
  ];
  
  module.exports = urls;


//   {
//     "_id": {
//         "$oid": "61170706f5285b77080595d6"
//     },
//     "id": {
//         "$numberLong": "1"
//     },
//     "type": "MIni",
//     "name": "index",
//     "url": "http://192.168.33.10:3000"
// }
// {
//   "_id": {
//       "$oid": "61170706f5285b77080595d6"
//   },
//   "id": {
//       "$numberLong": "2"
//   },
//   "type": "MIni",
//   "name": "2222222",
//   "url": "http://192.168.33.10:3000"
// }