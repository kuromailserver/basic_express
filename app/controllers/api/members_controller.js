const members = require('../../../db/dummy_data/Members');

exports.get_members = (req, res ,next)=>{
    res.json(members)
}

exports.get_member = (req, res ,next)=>{
    const found = members.some(member => member.id === parseInt(req.params.id))

    if (found){
        res.json(members.filter(member => member.id === parseInt(req.params.id)))
    } else {
        res.status(400).json({ msg: 'Member not found' });
    }
}
