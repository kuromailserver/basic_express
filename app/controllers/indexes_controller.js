const { Url } = require('../models/url');

exports.get_index = (req, res ,next)=>{
    const urls = require('../../db/dummy_data/Urls')

    res.render('index', {
        title: "index",
        urls: urls,
    })
}

exports.post_data = (req, res ,next)=>{
    const data = req.body.data;

    console.log("post data")
    console.log("data :" + data)
}

exports.get_index_from_db = async (req, res ,next)=>{
    console.log("get_index_from_db")

    const urls = await Url.find().lean();
    console.log("lists : " + JSON.stringify(urls))

    if (urls) {
        res.render('index', {
            title: "index data from db",
            urls: urls,
        })
    }
}