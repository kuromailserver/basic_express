const mongoose = require('mongoose');

// Url Schema
const urlSchema = mongoose.Schema({
  id: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  url: {
    type: String,
    required: true
  }
});
const Url = mongoose.model('Url', urlSchema);
module.exports.Url = Url;
