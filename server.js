require('dotenv').config();
const path = require('path');

const BACKEND_PORT = process.env.BACKEND_PORT || 3000

const express = require('express');
const listEndpoints = require('express-list-endpoints')
const exphbs = require('express-handlebars');

const connectDB = require('./db/connect_mongodb');

const app = express();

// logger
// const logger = require('morgan');
// app.use(logger('dev'));

const logger = require('./app/middleware/logger');
app.use(logger);

// Body parser
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

// view engine
app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');
app.set('views', path.join(__dirname, process.env.VIEW_ENGINE_PATH));

// route
app.use('/', require('./config/routes/indexes'));
app.use('/api/members', require('./config/routes/api/members'));
// auth

// Server listen
app.listen(BACKEND_PORT, function() {
  console.log('Example app listening on port 3000!');
  console.log(listEndpoints(app));
  console.log('http://192.168.33.10:3000');
  // console.log("process.env : " + JSON.stringify(process.env))
  connectDB()
});